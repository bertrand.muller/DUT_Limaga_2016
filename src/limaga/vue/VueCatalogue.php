<?php
/**
 * Created by PhpStorm.
 * User: thomas
 * Date: 15/01/16
 * Time: 16:02
 */

namespace limaga\vue;


class VueCatalogue extends AbstractVue{

    protected static $name = 'Catalogue';

    private $produits;

    public function __construct($produits)
    {
        $this->produits = $produits;
    }

    public function renderBody($connected){
        $html = '';
        if($connected){
            $html.= '<div class="container panel panel-default">
                        <div class="panel-body">
                            <h3>Catalogue</h3>
                        </div>
                     </div>
                     <div class="container">';

            foreach($this->produits as $produit){
                $html.= '<div class="panel panel-default">
                         <div class="panel-body">';

                $vue = $produit->article->getVue();
                $html.= $vue->renderClassique();
                $html.= '</div></div>';
            }

            $html.='</div>';
        }

        echo $html;
    }
}