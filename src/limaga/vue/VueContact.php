<?php

namespace limaga\vue;


/**
 * Classe VueContact
 * @package limaga\vue
 *
 * Classe pour la vue du contact
 */
class VueContact extends AbstractVue
{


    /**
     * @var string
     *      Nom de la classe
     */
    protected static $name = 'Contact';


    /**
     * Fonction permettant d'afficher le contenu
     * associe a la vue du contact
     *
     * @param $connected
     *      Booleen permettant de savoir si l'utilisateur est connecte ou non
     */
    protected function renderBody($connecte){
    
            $html = '<div class="container panel panel-default">
                        <div class="panel-body">
                            <h3>Contact</h3>
                        </div>
                      </div>

                      <div class="container">
                        <p>
                            Vous pouvez joindre la piscine de Limaga à partir des coordonnées suivantes :<br/><br/>

                            &emsp;&emsp;<b>Adresse :</b> 20, rue de la Piscine<br/>
                            &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;Commune de Limaga<br/><br/>

                            &emsp;&emsp;<b>Téléphone :</b> 09 24 55 35<br/><br/>

                            &emsp;&emsp;<b>Email :</b> piscine@limaga.fr
                        </p>



                      </div>';
        echo $html;

    }

}