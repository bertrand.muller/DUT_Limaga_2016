<?php

namespace limaga\vue;


/**
 * Classe VueFacture
 * @package limaga\vue
 *
 * Classe pour une vue d'une facture
 */
class VueFacture extends AbstractVue
{

    /**
     * @var string
     *      Nom de la classe
     */
    protected static $name = 'Facture';


    /**
     * @var
     *      Facture traitee par cette vue
     */
    private $facture;


    /**
     * Constructeur pour une vue d'une facture
     *
     * @param $f
     *      Facture associee a la vue
     */
    public function __construct($f)
    {
        $this->facture = $f;
    }


    /**
     * Fonction permettant d'afficher le contenu
     * associe a la vue d'une facture
     *
     * @param $connecte
     *      Booleen permettant de savoir si l'utilisateur est connecte ou non
     */
    protected function renderBody($connecte)
    {

        $html = '';

        if($connecte) {

            $client = $this->facture->client();

            $html .= '<div class="container panel panel-default">
                        <div class="panel-body">
                            <h3>Détail de la Facture n°' . $this->facture->num . '</h3>
                        </div>

                        <div class="container">
                            <div class="row">
                                <div class="col-md-3">Facture n°: ' . $this->facture->num . '</div>
                                <div class="col-md-3">Date: ' . $this->facture->date . '</div>
                            </div>
                            <div class="row">
                                <div class="col-md-2">Client: ' . $client->prenom . ' ' . $client->nom . '</div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">Adresse: ' . $client->addresse . '</div>
                            </div>
                            <div class="row">
                                <div class="col-md-2">Téléphone: ' . $client->numTelephone . '</div>
                            </div><br/><br/>

                            <div class="row">
                                <div class="col-md-2">Total HT: ' . $this->facture->totalHT . '</div>
                            </div>

                            <div class="row">
                                <div class="col-md-2">Taux de TVA: 20%</div>
                            </div>

                            <div class="row">
                                <div class="col-md-2">Total TTC: ' . $this->facture->totalTTC . '</div>
                            </div><br/><br/>

                            <div class="row">
                                <div class="col-md-2">Prestations facturées: </div>
                            </div>

                            <table class="table">
                                <tr>
                                    <th>Prestation</th>
                                    <th>Prix unitaire HT</th>
                                    <th>Quantité</th>
                                    <th>TVA</th>
                                    <th>Prix TTC</th>
                                </tr>';

                                    foreach($this->facture->articles as $article) {
                                        $html.= $article->getVue()->renderFacture($article->pivot->qte);
                                    }
            $html.= '    </table>
                     </div>';
            echo $html;
        }
    }
}