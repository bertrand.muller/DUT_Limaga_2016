<?php

namespace limaga\vue;


/**
 * Classe VueNavigation
 * @package limaga\vue
 *
 * Classe pour la vue d'une navigation
 */
class VueNavigation {


    /**
     * @var array
     *      Tableau de liens pour le menu de navigation
     */
    private $links;


    /**
     * Constructeur d'une vue pour la navigation
     *
     * @param $mode
     *      Permet si l'utilisateur est connecte ou non
     *      Pour afficher les bons liens en consequence
     */
    public function __construct($mode){
        switch($mode) {
            case DECONNECTE:
                $this->links = array(
                    'Accueil'=>'/accueil',
                    'Contact'=>'/contact',
                    'Connexion'=>'/connexion'
                );
                break;
            case CONNECTE:
                $this->links = array(
                    'Accueil'=>'/accueil',
                    'Contact'=>'/contact',
                    'Réserver'=>array('Ebillet'=>'/reserver/Ebillet',
                    'Eabonnement'=>'/reserver/Eabonnement'),
                    'Catalogue'=> '/achat',
                    'Panier'=>'/panier',
                    'Espace Personnel'=>'/perso',
                    'Déconnexion'=>'/deconnexion'
                );
                break;
        }
    }


    /**
     * Fonction permettant d'afficher le contenu
     * associe a la vue pour la navigation
     */
    public function render($active){
        $html ='
            <nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand text-center" href="#">Limaga</a>
                </div>

            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
        ';
        foreach($this->links as $name=>$link) {
            $class = "";
            if (strcmp($active, $name) == 0) {
                $class = "class=\"active\"";
            }
            if (gettype($link) === 'string') {
                $html .= '<li ' . $class . '><a href=".' . $link . '">' . $name . '</a></li>';

            }else{

                $html .= '<li ' . $class . ' class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">'.$name.'
                <span class="caret"></span></a>
                <ul class="dropdown-menu">';
                foreach($link as $name1=>$link1) {
                    $html .= '<li><a href=".' . $link1 . '">' . $name1 . '</a></li>';
                }
                $html .= '</li></ul>';
            }
        }
        $html.='</ul></div></div></nav>';
        echo $html;
    }
}