<?php

namespace limaga\vue;


/**
 * Classe VueConnexion
 * @package limaga\vue
 *
 * Classe pour la vue de la connexion
 */
class VueConnexion extends AbstractVue{


    /**
     * @var string
     *      Nom de la classe
     */
    protected static $name='Connexion';


    /**
     * Fonction permettant d'afficher le contenu
     * associe a la vue d'une connexion
     *
     * @param $connecte
     *      Booleen permettant de savoir si l'utilisateur est connecte ou non
     */
    public function renderBody($connecte){

        if($connecte)
        {
            echo 'Vous êtes déjà connecté';
        }else{
            include('web/tpl/connexionForm.html');
            include('web/tpl/inscriptionForm.html');
        }
    }
}