<?php

namespace limaga\vue;


/**
 * Classe VuePanier
 * @package limaga\vue
 *
 * Classe pour la vue d'un panier
 */
class VuePanier extends AbstractVue{


    /**
     * @var string
     *      Nom de la classe
     */
    protected static $name = 'Panier';


    /**
     * @var
     *      Articles contenus dans le panier
     */
    protected $articles;


    /**
     * Constructeur d'une vue pour un panier
     *
     * @param $articles
     *      Articles contenus dans le panier
     */
    public function __construct($articles){
        $this->articles = $articles;
    }


    /**
     * Fonction permettant d'afficher le contenu
     * associe a la vue d'un panier
     *
     * @param $connecte
     *      Booleen permettant de savoir si l'utilisateur est connecte ou non
     */
    public function renderBody($connecte){
        $html = '';
        if($connecte){
            $html.='<div class="container">';
            foreach ($this->articles as $article)
            {
                $html.='<div class="panel panel-default">
                    <div class="panel-body">
                        <div class="row">';
                $html.= $article->getVue()->renderPanier($article->pivot->qte);
                $html.= '<form class="col-xs-6 col-sm-4 col-md-2" method="post" action="./panier/'.$article->numArt.'">';
                $html.= '<button style="width:100%" name="commander" value="suppression" class="btn btn-primary">Supprimer</button>';
                $html.= '</form>';
                $html.='</div></div></div>';
            }


            if(count($this->articles)>0){
                $html.='<form method="post" action="./panier" class="col-xs-6 col-xs-offset-6 col-sm-4 col-sm-offset-8 col-md-2 col-md-offset-10">';
                    $html.='<button style="width:100%" name="commander" value="f3" class="btn btn-primary">Valider le panier</button>';
                $html.='</form>';
            }else{
                $html.='<div class="panel panel-default">
                    <div class="panel-body">';
                $html.='<p>Votre panier est vide ! Ajoutez des articles et vous pourrez les visualiser dans cette section.</p>';
                $html.='</div></div>';
            }

            $html.='</div>';
        }
        $html.=
            '
            <script text="javascript">
                $(\':input\').bind(\'click keyup\', function(){
                    var prixId = $(this).attr(\'id\');
                   $.post(".achat/"+prixId, { newQte : $(this).val() }).done(function(data){
                                $(\'#prix\'+prixId+\'\').html(data + \' €\');
                    });
                });
            </script>
            ';
        echo $html;
    }
}