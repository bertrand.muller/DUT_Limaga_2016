<?php

namespace limaga\vue;


/**
 * Classe VueEspacePersonnel
 * @package limaga\vue
 *
 * Classe pour la vue de l'espace personnel
 */
class VueEspacePersonnel extends AbstractVue
{


    /**
     * @var string
     *      Nom de la classe
     */
    protected static $name = 'Espace Personnel';


    /**
     * @var
     *      Client associe a son espace personnel
     */
    private $client;


    /**
     * Constructeur pour une vue d'un espace personnel
     *
     * @param $c
     *      Client de l'espace personnel
     */
    public function __construct($c)
    {
        $this->client = $c;
    }


    /**
     * Fonction permettant d'obtenir le contenu
     * associe a la vue d'un espace personnel
     *
     * @param $connecte
     *      Booleen permettant de savoir si l'utilisateur est connecte ou non
     */
    protected function renderBody($connecte)
    {

        $html = '';

        if($connecte) {
            $html .= $this->obtenirInformationsClient();
            $html .= $this->obtenirFacturesClient();
            $html .= $this->obtenirEbilletsClient();
            $html .= $this->obtenirEabonnementsClient();
        }

        echo $html;

    }


    /**
     * Fonction permettant d'obtenir le contenu
     * des informations du client de cet espace personnel
     */
    public function obtenirInformationsClient() {

        $html = '
                 <div class="container">
                     <div class="panel panel-default">
                        <div class="panel-body">
                            <h3>Mes Informations</h3>
                            <div class="row">
                                <div class="col-sm-4 col-md-4"><b>Nom :</b> '.$this->client->nom.'</div>
                                <div class="col-sm-3 col-md-3"><b>Prénom :</b>'.$this->client->prenom.'</div>
                                <div class="col-sm-5 col-md-5"><b>Date de Naissance :</b> '.$this->client->dateNaissance.'</div>

                                <div class="col-sm-6 col-md-6"><b>Adresse :</b>'.$this->client->addresse.'</div>
                                <div class="col-sm-6 col-md-6"><b>Email :</b> '.$this->client->email.'</div>

                                <div class="col-sm-6 col-md-6"><b>Téléphone :</b> '.$this->client->numTelephone.'</div>
                                <div class="col-sm-6 col-md-6"><b>Niveau de Natation :</b> '.$this->client->getNiveauNatation().'</div>
                            </div>
                            <br/>
                        </div>
                    </div>
                  </div><br/>';

        return $html;

    }


    /**
     * Fonction permettant d'obtenir le contenu
     * des factures qui sont associees au client
     */
    public function obtenirFacturesClient() {

        $html = '';
        $tabFactures = $this->client->factures;

        if(count($tabFactures) != 0) {

            $html .= '<div class="container">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <h3>Mes Factures</h3>
                            </div>
                        </div>';

            foreach ($tabFactures as $fact) {

                $html .= '
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-sm-3">Facture n°: ' . $fact->num . '</div>
                                        <div class="col-sm-3">Date: ' . $fact->date . '</div>

                                        <div class="col-sm-3">Total TTC: ' . $fact->totalTTC . '€</div>';

                                if($fact->reglee != 0) {
                                    $part = $fact->resteAPaye/$fact->reglee;
                                    $html .= '<div class="col-sm-3">Reste à payer: ' . $fact->reglee . 'x '. $part . '€</div>';
                                }


                $html .= '</div>
                          <div class="row">
                            <div class="col-sm-12">Type de paiement choisi: '. $fact->typePaiement .'</div>
                            <div class="col-sm-12">
                            </br>
                                <form method="post" action="./perso/fact/'. $fact->num .'" class="col-xs-6 col-xs-offset-6 col-sm-4 col-sm-offset-8 col-md-2 col-md-offset-10">
                                    <button style="width:100%" name="afficherfact" value="f3" class="btn btn-primary">Voir la facture</button>
                                </form>
                            </div>
                          </div></div></div>';

            }
        }

        $html .= '</div>';

        return $html;


    }


    /**
     * Fonction permettant d'obtneir le contenu
     * des E-Billets appartenant au client
     */
    public function obtenirEbilletsClient(){
        $ebillets = $this->client->ebillets()->orderBy('date','desc')->get();
        $html = '';
        if(count($ebillets) > 0) {
            $html .= '<div id="#ebillets" class="container">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <h3>Mes E-Billets</h3>
                            </div>
                        </div>';
            $html .= '<div class="row">';
            foreach ($ebillets as $ebillet) {
                $html .= '<div class="panel panel-default">
                            <div class="panel-body">';
                $html .= $ebillet->article->getVue()->renderClassique();
                $html .= '</div></div>';
            }
            $html .= '</div></div>';
        }
        return $html;
    }


    /**
     * Fonction permettant d'obtenir le contenu
     * des E-Abonnements appartenant au client
     */
    public function obtenirEabonnementsClient(){
        $html = '';
        $eabonnements = $this->client->eabonnements;
        if(count($eabonnements) > 0) {
            $html .= '<div id="#e-abonnements" class="container">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <h3>Mes E-Abonnements</h3>
                            </div>
                        </div>';
            $html .= '<div class="row">';
            foreach ($eabonnements as $eabonnement) {
                $html .= '<div class="panel panel-default">
                            <div class="panel-body">';
                $html .= $eabonnement->article->getVue()->renderClassique();
                $html .= '</div></div>';
            }
            $html .= '</div></div>';
        }
        return $html;
    }
}