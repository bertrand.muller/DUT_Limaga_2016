<?php

namespace limaga\vue;


/**
 * Classe AbstractVue
 * @package limaga\vue
 *
 * Classe abstraite pour les vues
 */
abstract class AbstractVue {


    /**
     * Fonction permettant d'afficher l'en-tete,
     * le contenu associe a la vue correspondante
     *
     * @param bool|false $connecte
     *      Booleen permettant de savoir si l'utilisateur est connecte ou non
     */
    public function render($connecte = false){
        include('web/tpl/header.html');
        $nav = new VueNavigation($connecte);
        $nav->render(static::$name);
        $this->renderBody($connecte);
        include('web/tpl/footer.html');
    }


    /**
     * Fonction permettant d'obtenir le contenu
     * associe a la vue correspondante
     *
     * @param $connecte
     *      Booleen permettant de savoir si l'utilisateur est connecte ou non
     *
     * @return mixed
     *      Chaine de caracteres avec le contenu associe a la vue
     */
    protected abstract function renderBody($connecte);
}