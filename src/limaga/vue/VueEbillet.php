<?php

namespace limaga\vue;


/**
 * Classe VueEbillet
 * @package limaga\vue
 *
 * Classe pour une vue des E-Billets
 */
class VueEbillet extends AbstractVue
{

    /**
     * @var string
     *      Nom de la classe
     */
    protected static $name = 'EBillet';


    /**
     * @var
     *      Ensemble de E-Billets
     */
    private $ebillets;


    /**
     * Constructeur pour la vue des E-Billets
     *
     * @param $ebillets
     *      Ensemble des E-Billets associes a cette vue
     */
    public function __construct($ebillets)
    {
        $this->ebillets = $ebillets;
    }


    /**
     * Fonction permettant d'afficher le contenu
     * associe a la vue du contact
     *
     * @param $connected
     *      Booleen permettant de savoir si l'utilisateur est connecte ou non
     */
    public function renderBody($connected)
    {
        $html= '';
        if($connected)
        {
            foreach ($this->ebillets as $ebillet) {
                $html.= $this->renderEBillet($ebillet);
            }
        }
        echo $html;
    }


    /**
     * Fonction permettant d'obtenir le contenu
     * associe a un E-Billet en particulier
     *
     * @param $ebillet
     *      E-Billet dont on veut avoir la description
     */
    private function renderEBillet($ebillet)
    {
        $date = new \DateTime($ebillet->date);
        $html = '<div class="container">';
        $html.= '<h3 style="text-align: center">Complexe aquatique de LIMAGA</h3>';
        $html.= '<h3 style="text-align: center">Billet d\'entrée</h3>';
        $html.= '<div class="col-md-12">N° de e-billet : '.$ebillet->numArt.'</div>';
        $html.= '<div class="col-md-12">Nom : '.$_SESSION['client']->nom.'</div>';
        $html.= '<div class="col-md-12">Prénom : '.$_SESSION['client']->prenom.'</div>';
        $html.= '<div class="col-md-12"><p>';
        $html.= 'Billet d\'entrée pour le '.$date->format('d/m/Y').' à '.$date->format('H').'h<br/>';
        $html.= 'Ce billet vous permet d\'obtenir un bracelet qui vous donnera accès à la piscine<br/>';
        $html.= 'Vous pouvez obtenir ce bracelet en échange de ce e-billet à une caisse ou l\'obtenir directement à<br/>';
        $html.= 'une caisse automatique en utilisant le code-barre ci-dessous.<br/>';
        $html.= 'CODE-BARRE';
        $html.= '</p></div>';
        $html.= '</div>';
        return $html;
    }
}