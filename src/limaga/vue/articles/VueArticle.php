<?php

namespace limaga\vue\articles;


/**
 * Classe VueArticle
 * @package limaga\vue\articles
 *
 * Vue liee a un article
 */
abstract class VueArticle {


    /**
     * @var Article associe
     */
    protected $article;


    /**
     * Constructeur pour une vue d'un article
     *
     * @param $article
     * Article associe
     */
    public function __construct($article){
        $this->article = $article;
    }


    /**
     * Fonction abstraite permettant de retourner
     * un affichage classique pour un produit specifique
     *
     * @return mixed
     *      Chaine de caracteres correspondaant a l'affichage
     */
    public abstract function renderClassique();


    /**
     * Fonction permettant de retourner
     * un affichage d'un produit dans une facture
     *
     * @param $qte
     *      Quantite du produit
     *
     * @return mixed
     *      Chaine de caracteres correspondant a l'affichage
     */
    public abstract function renderFacture($qte);


    /**
     * Fonction permettant de retourner
     * un affichage d'un produit dans le panier
     *
     * @param $qte
     *      Quantite du produit
     *
     * @return mixed
     *      Chaine de caracteres correspondant a l'affichage
     */
    public abstract function renderPanier($qte);


    /**
     * Fonction permettant de retourner une date
     * sous le format ....
     *
     * @param $date
     *      Date sous le format ....
     *
     * @return string
     *      Nouvelle date sous le format ....
     */
    protected function renderDate($date){
        $date = explode(" ",$date);
        $jour = explode("-",$date[0]);
        $heure = explode(":",$date[1]);
        $str ="";
        array_reverse($jour);

        $str .= "$jour[0]/$jour[1]/$jour[2] à ".($heure[0]+0)."h".$heure[1];
        return $str;
    }
}