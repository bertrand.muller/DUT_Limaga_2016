<?php

namespace limaga\vue\articles;


/**
 * Class VueEbillet
 * @package limaga\vue\articles
 *
 * Vue pour un E-Billet
 */
class VueEbillet extends VueArticle{


    /**
     * Fonction abstraite permettant de retourner
     * un affichage classique pour un E-Billet
     *
     * @return mixed
     *      Chaine de caracteres correspondaant a l'affichage d'un E-Billet
     */
    public function renderClassique(){
        $date = new \DateTime($this->article->date);
        $html = '<div class="container">';
        $html.= '<h3 style="text-align: center">Complexe aquatique de LIMAGA</h3>';
        $html.= '<h3 style="text-align: center">Billet d\'entrée</h3>';
        $html.= '<div class="col-md-12">N° de e-billet : '.$this->article->numArt.'</div>';
        $html.= '<div class="col-md-12">Nom : '.$_SESSION['client']->nom.'</div>';
        $html.= '<div class="col-md-12">Prénom : '.$_SESSION['client']->prenom.'</div>';
        $html.= '<div class="col-md-12"><p>';
        $html.= 'Billet d\'entrée pour le '.$date->format('d/m/Y').' à '.$date->format('H').'h<br/>';
        $html.= 'Ce billet vous permet d\'obtenir un bracelet qui vous donnera accès à la piscine<br/>';
        $html.= 'Vous pouvez obtenir ce bracelet en échange de ce e-billet à une caisse ou l\'obtenir directement à<br/>';
        $html.= 'une caisse automatique en utilisant le code-barre ci-dessous.<br/>';
        $html.= 'CODE-BARRE';
        $html.= '</p></div>';
        $html.= '</div>';
        return $html;
    }


    /**
     * Fonction permettant de retourner
     * un affichage d'un E-Billet dans une facture
     *
     * @param $qte
     *      Quantite du E-Billet
     *
     * @return mixed
     *      Chaine de caracteres correspondant a l'affichage du E-Billet
     */
    public function renderFacture($qte){
        $prix = $this->article->prix();
        $html = '<tr>';
        $html.= '<td>e-billet [ '.$this->article->date.' ]</td>';
        $html.= '<td>'.$prix.'</td>';
        $html.= '<td>'.$qte.'</td>';
        $html.= '<td>'.(TVA*100).'%</td>';
        $html.= '<td>'.($prix + $prix*TVA)*$qte.'</td>';
        $html.= '</tr>';
        return $html;
    }


    /**
     * Fonction permettant de retourner
     * un affichage d'un E-Billet dans le panier
     *
     * @param $qte
     *      Quantite du E-Billet
     *
     * @return mixed
     *      Chaine de caracteres correspondant a l'affichage du E-Billet
     */
    public function renderPanier($qte){
        $html ='<div class="col-xs-6 col-md-2">'.$qte.' E-Billet</div>';
        $html.='<div class="col-xs-6 col-md-4">'.$this->renderDate($this->article->date).'</div>';
        $html.='<div class="col-xs-6 col-md-4">'.$this->article->prix() * $qte.' €</div>';
        return $html;
    }
}