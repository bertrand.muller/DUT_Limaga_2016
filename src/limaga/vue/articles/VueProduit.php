<?php

namespace limaga\vue\articles;


/**
 * Class VueProduit
 * @package limaga\vue\articles
 *
 * Vue pour un produit
 */
class VueProduit extends VueArticle{


    /**
     * Fonction abstraite permettant de retourner
     * un affichage classique pour un produit
     *
     * @return mixed
     *      Chaine de caracteres correspondaant a l'affichage pour un produit
     */
    public function renderClassique(){
        $html = '<div class="row">';
        $html.= '<div class="col-md-4">';
        $html.= '<form action="index.php/achat/'.$this->article->numArt.'" method="post">';
        $html.= '<div>'.$this->article->libelle.'</div>';
        $html.= '<div>Prix HT : '.$this->article->prix().' €</div>';
        $html.= '<div>Prix TTC : '.($this->article->prix()+$this->article->prix()*TVA).' €</div>';
        $html.= '<input style="margin-bottom: 2%" name="qte" min="1" value="1" type="number" class="form-control">';
        $html.= '<button class="btn btn-primary" name="acheter" value="produit">Acheter</button>';
        $html.= '</form>';
        $html.= '</div>';
        $html.= '</div>';
        return $html;
    }


    /**
     * Fonction permettant de retourner
     * un affichage d'un produit dans le panier
     *
     * @param $qte
     *      Quantite du produit
     *
     * @return mixed
     *      Chaine de caracteres correspondant a l'affichage
     */
    public function renderPanier($qte){
        $html = '<div class="col-xs-6 col-md-2">'.$this->article->libelle.'<input id="'.$this->article->numArt.'" class="form-control" type="number" value="'.$qte.'" min="1"></div>';
        $html.= '<div class="col-xs-6 col-md-4 col-md-offset-4" id="prix'.$this->article->numArt.'">'.$this->article->prix() * $qte.' €</div>';
        return $html;
    }


    /**
     * Fonction permettant de retourner
     * un affichage d'un produit dans une facture
     *
     * @param $qte
     *      Quantite du produit
     *
     * @return mixed
     *      Chaine de caracteres correspondant a l'affichage pour un produit
     */
    public function renderFacture($qte){
        $prix = $this->article->prix();
        $html = '<tr>';
        $html.= '<td>'.$this->article->libelle.'</td>';
        $html.= '<td>'.$prix.'</td>';
        $html.= '<td>'.$qte.'</td>';
        $html.= '<td>'.(TVA*100).'%</td>';
        $html.= '<td>'.($prix + $prix*TVA)*$qte.'</td>';
        $html.= '</tr>';
        return $html;
    }
}