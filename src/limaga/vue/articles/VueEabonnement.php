<?php

namespace limaga\vue\articles;


/**
 * Classe VueEabonnement
 * @package limaga\vue\articles
 *
 * Vue liee a un E-Abonnement
 */
class VueEabonnement extends VueArticle{


    /**
     * Fonction abstraite permettant de retourner
     * un affichage classique pour un E-Abonnement
     *
     * @return mixed
     *      Chaine de caracteres correspondaant a l'affichage d'un E-Abonnement
     */
    public function renderClassique(){
        $html = '<div class="container">';
        $html.= '<h3 style="text-align: center">Complexe aquatique de LIMAGA</h3>';
        $html.= '<h3 style="text-align: center">E-Abonement</h3>';
        $html.= '<div class="col-md-12">N° d\'abonnement : '.$this->article->numArt.'</div>';
        $html.= '<div class="col-md-12">Nombre d\'entrées comprises dans l\'abonnement : '.$this->article->NbPlace.' ('.$this->article->NbRestant.' restantes)</div>';
        $html.= '<div class="col-md-12">Nom : '.$_SESSION['client']->nom.'</div>';
        $html.= '<div class="col-md-12">Prénom : '.$_SESSION['client']->prenom.'</div>';
        $html.= '<div class="col-md-12"><p>';
        $html.= 'Vous pouvez obtenir ce bracelet à '.$this->article->NbRestant.' entrée(s) en échange de ce e-billet à une caisse ou l\'obtenir directement à<br/>';
        $html.= 'une caisse automatique en utilisant le code-barre ci-dessous.<br/>';
        $html.= 'CODE-BARRE';
        $html.= '</p></div>';
        $html.= '</div>';
        return $html;
    }


    /**
     * Fonction permettant de retourner
     * un affichage d'un E-Abonnement dans le panier
     *
     * @param $qte
     *      Quantite du E-Abonnement
     *
     * @return mixed
     *      Chaine de caracteres correspondant a l'affichage du E-Abonnement
     */
    public function renderPanier($qte){
        $html ='<div class="col-xs-6 col-md-2">'.$qte.' E-Abonnement</div>';
        $html.='<div class="col-xs-6 col-md-4">Nombre de places : '.$this->article->NbPlace.'</div>';
        $html.='<div class="col-xs-6 col-md-4" id="prix'.$this->article->numArt.'">'.$this->article->prix() * $qte.' €</div>';
        return $html;
    }


    /**
     * Fonction permettant de retourner
     * un affichage d'un E-Abonnement dans une facture
     *
     * @param $qte
     *      Quantite du E-Abonnement
     *
     * @return mixed
     *      Chaine de caracteres correspondant a l'affichage du E-Abonnement
     */
    public function renderFacture($qte){
        $prix = $this->article->prix();
        $html = '<tr>';
        $html.= '<td>e-abonnement [ '.$this->article->NbPlace.' ]</td>';
        $html.= '<td>'.$prix.'</td>';
        $html.= '<td>'.$qte.'</td>';
        $html.= '<td>'.(TVA*100).'%</td>';
        $html.= '<td>'.($prix + $prix * TVA)*$qte.'</td>';
        $html.= '</tr>';
        return $html;
    }

}