<?php

namespace limaga\vue;

/**
 * Classe VueReserver
 * @package limaga\vue
 *
 * Classe pour la vue d'une reservation
 */
class VueReserver extends AbstractVue{


    /**
     * @var string
     *      Nom de la classe
     */
    protected static $name = 'Réserver';


    /**
     * @var
     *      Mode de reservation que le client a choisi
     */
    private $mode;


    /**
     * Constructeur d'une vue pour la reservation
     *
     * @param $mode
     *      Mode de reservation
     */
    public function __construct($mode){
        $this->mode = $mode;
    }


    /**
     * Fonction permettant d'afficher le contenu
     * associe a la vue d'une reservation
     *
     * @param $connected
     *      Booleen permettant de savoir si l'utilisateur est connecte ou non
     */
    public function renderBody($connecte){
        if($connecte){
            if($this->mode == 'Ebillet')
                include('web/tpl/achatEbillet.html');
            else if($this->mode == 'Eabonnement')
                include('web/tpl/achatEabonnement.html');
        }else{
            echo "Vous n'êtes pas connecté";
        }
    }
}