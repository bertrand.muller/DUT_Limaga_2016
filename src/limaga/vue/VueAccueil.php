<?php
/**
 * Created by PhpStorm.
 * User: thomas
 * Date: 14/12/15
 * Time: 09:40
 */

namespace limaga\vue;

use limaga\models\Ebillet;

class VueAccueil extends AbstractVue{
    protected static $name = 'Accueil';

    protected function renderBody($connecte){
            include('web/tpl/accueil.html');
    }
}