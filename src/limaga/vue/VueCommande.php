<?php

namespace limaga\vue;


/**
 * Classe VueCommande
 * @package limaga\vue
 *
 * Classe pour la vue d'une commande
 */
class VueCommande extends AbstractVue{


    /**
     * @var string
     *      Nom de la classe
     */
    protected static $name = 'Commande';


    /**
     * @var
     *      Facture associee a la commande
     */
    protected $facture;


    /**
     * Constructeur d'une vue pour une commande
     *
     * @param $facture
     *      Facture associee a la commande
     */
    public function __construct($facture){
        $this->facture = $facture;
    }


    /**
     * Fonction permettant d'afficher le contenu
     * associe a la vue d'une commande
     *
     * @param $connected
     *      Booleen permettant de savoir si l'utilisateur est connecte ou non
     */
    public function renderBody($connected){
        $html = '';
        if($connected){
            $html.='<div class="container">
                    <div class="panel panel-default">
                    <div class="panel-body">
                    <div class="row">';
            $html.= $this->renderFactureHeader();

            $html.= $this->renderPrestationTable();
            $html.= $this->renderFooter();
            $html.='</div></div></div></div>';
        }
        echo $html;
    }


    /**
     * Fonction permettant d'obtenir le contenu du footer
     * associe a la vue correspondante
     *
     * @return mixed
     *      Chaine de caracteres avec le contenu associe au footer de la vue d'une commande
     */
    private function renderFooter(){
        $html = '<div class="col-sm-12">Total HT : '.$this->facture->totalHT.' €</div>';
        $html.= '<div class="col-sm-12">Total TTC : '.$this->facture->totalTTC.' €</div>';
        $html.= '<div class="col-sm-12">Type de payement :</div>';
        $html.= '<form <div class="col-sm-12" method="post" action="./panier">';
        $html.= '<div class="radio"><label><input type="radio" name="mode" value="direct">Direct</label></div>';
        $html.= '<div class="radio"><label><input type="radio" name="mode" value="diff">Différé</label></div>';
        $html.= '<button class="btn btn-success col-sm-offset-10" name="valider" value="f4">Payer</button>';
        $html.= '</form>';
        return $html;
    }


    /**
     * Fonction permettant d'obtenir le contenu de la prestation
     * associee a la vue correspondante sous la forme de tableau
     *
     * @return mixed
     *      Chaine de caracteres avec le contenu de la prestation associee a la vue
     */
    private function renderPrestationTable(){
        $c = $_SESSION['client'];
        $html = '<table class="table">';
        $html.= '<tr>';
        $html.= '<th>Prestation facturée</th>';
        $html.= '<th>Prix unitaire</th>';
        $html.= '<th>Quantité</th>';
        $html.= '<th>TVA</th>';
        $html.= '<th>Prix TTC</th>';
        $html.= '</tr>';
        foreach($c->articles as $article){
            $html.= $article->getVue()->renderFacture($article->pivot->qte);
        }
        $html.= '</table>';
        return $html;
    }


    /**
     * Fonction permettant d'obtenir le contenu du header d'une facture
     * associee a la vue d'une commande
     *
     * @param $connecte
     *      Booleen permettant de savoir si l'utilisateur est connecte ou non
     *
     * @return mixed
     *      Chaine de caracteres avec le contenu du header de la facture associee a la vue d'une commande
     */
    private function renderFactureHeader(){
        $c = $_SESSION['client'];
        $html='<div class="col-sm-6">Facture n° '.$this->facture->num.'</div>';
        $html.='<div class=" col-sm-6">Date : '.date('d-m-Y').'</div>';
        $html.='<div class="col-sm-2">Client :</div>';
        $html.='<div class="col-sm-4">Nom : '.$c->nom.'</div>';
        $html.='<div class="col-sm-4">Prenom : '.$c->prenom.'</div>';
        $html.='<div class="col-sm-5">Adresse : '.$c->addresse.'</div>';
        $html.='<div class="col-sm-5">Téléphone : '.$c->numTelephone.'</div>';
        return $html;
    }
}