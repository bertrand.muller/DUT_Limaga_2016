<?php

namespace limaga\controllers;


use limaga\vue\AbstractVue;
use limaga\vue\VueContact;

class ControllerContact extends AbstractController
{

    /**
     * Methode qui permet d'afficher les informations de contact
     * en creant la vue associee
     */

    public function afficherInformationsContact() {

        $vue = new VueContact();
        $vue->render($this->isConnected());

    }

}