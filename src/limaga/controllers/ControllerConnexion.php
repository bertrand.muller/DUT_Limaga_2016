<?php

namespace limaga\controllers;


use limaga\vue\VueConnexion as VueConnexion;
use limaga\models\Client as Client;

class ControllerConnexion extends AbstractController{

    /**
     * Mehode qui permet d'afficher le formulaire de connexion en
     * creant la vue associee
     */

    public function formulaireConnexion(){
        $vue = new VueConnexion();
        $vue->render($this->isConnected());
    }



    /**
     * Methode qui permet a un utilisateur de se connecter en
     * verifiant ses identifiants
     */

    public function seConnecte(){
        $post = $this->request->post();
        if(isset($post['connexion']) && $post['connexion']=='f1') {
            $res = Client::whereRaw('email=? and mdp=?',
                array(
                $post['email'],
                MD5($post['mdp'])
            ))->get();

            if(count($res) == 1) {
                $_SESSION['client'] = $res[0];
                \Slim\Slim::getInstance()->redirectTo('accueil');
            }else{
                echo 'Utilisateur introuvable';
            }
        }
    }
}