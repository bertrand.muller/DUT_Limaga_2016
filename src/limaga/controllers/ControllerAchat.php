<?php

namespace limaga\controllers;


use limaga\models\Produit;
use limaga\models\Client;
use limaga\vue\VueAchat;
use limaga\vue\VueCatalogue;

class ControllerAchat extends AbstractController{

    /**
     * Methode qui permet de lister l'ensemble des produit en
     * creant la vue associee
     */

    public function listerProduits(){
        $listeDeProduit = Produit::all();
        $v = new VueCatalogue($listeDeProduit);
        $v->render($this->isConnected());
    }



    /**
     * Methode qui permet d'ajouter un produit dans le panier
     *
     * @param $numArt
     *
     *      Numero de l'article a ajouter dans le panier
     */

    public function acheterProduit($numArt){
        if($this->isConnected()) {
            $data = $this->request->post();
            if (isset($data['acheter']) && $data['acheter'] == 'produit') {
                $client = $_SESSION['client'];
                $save = false;
                /* Recherche si l'article est déjà présent dans le panier */
                foreach($client->articles as $article){
                    if($article->numArt == $numArt){
                        $article->pivot->qte += $data['qte']; /* Si oui on incremente la quantité */
                        $article->pivot->save();
                        $save = true;
                        break;
                    }
                }
                /* Si l'article n'existe pas dans le panier */
                if(!$save)
                    $client->articles()->attach($numArt,array("qte" => $data['qte']));
                $_SESSION['client'] = Client::find($client->email);
                \Slim\Slim::getInstance()->redirectTo('panier');
            }

            if(isset($data['newQte'])){
                $client = $_SESSION['client'];
                foreach($client->articles as $article){
                    if($article->numArt == $numArt){
                        $article->pivot->qte = $data['newQte'];
                        $article->pivot->save();
                        echo $article->typeable->prix()*$article->pivot->qte;
                        break;
                    }
                }
            }
        }
    }
}