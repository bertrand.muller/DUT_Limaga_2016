<?php

namespace limaga\controllers;


use limaga\models\Facture;
use limaga\vue\VueCommande;
use limaga\vue\VuePanier;
use limaga\vue\VueEbillet;
use limaga\models\Client;

class ControllerPanier extends AbstractController{

    /**
     * Merhode permettant de lister les articles du panier
     * en creant la vue associee
     */

    public function lister(){
        if($this->isConnected()){
            $c = $_SESSION['client'];
            $articles = $c->articles;
            $v = new VuePanier($articles);
            $v->render(true);
        }
    }




    /**
     * Methode qui permet de valider un panier afin de proceder a son
     * paiement
     */

    public function commander(){
        if($this->isConnected()) {
            $data = $this->request->post();
            if (isset($data['commander']) && $data['commander'] == 'f3')
            {
                $this->preparerFacture($data);
            }
            else if(isset($data['valider']) && $data['valider'] == 'f4')
            {
                if(isset($_SESSION['facture_en_cours']) && !empty($_SESSION['facture_en_cours']))
                {
                    $this->validerFacture($data);
                }
            }
        }
    }



    /**
     * Methode qui permet de preparer une facture apres la validation du panier
     *
     * @param $data
     *
     *      Requete du type POST contenant les informations requises
     */

    private function preparerFacture($data){
        $first =  Facture::orderBy('num', 'desc')->first();
        $f = new Facture();
        if($first != null)
            $f->num = $first->num + 1;
        else
            $f->num = 0;
        $f->email = $_SESSION['client']->email;
        $f->date = date('Y-m-d');
        $f->totalHT = 0;
        $f->totalTTC = 0;
        $f->reglee = 1;
        $indispo = false;

        foreach($_SESSION['client']->articles as $art)
        {
            $f->totalHT += $art->pivot->qte * $art->typeable->prix();
            $f->totalTTC+= $art->pivot->qte * ($art->typeable->prix() + $art->typeable->prix() * TVA);
            if($art->type == 'Ebillet'){
                if(!$art->typeable->estDisponible()){
                    $indispo = true;
                    break;
                }
            }
        }

        if(!$indispo)
        {
            $_SESSION['facture_en_cours'] = $f;
            $v = new VueCommande($f);
            $v->render($this->isConnected());
        }else{
            echo '<div class="alert alert-danger">Votre commande ne peut être validée<br/>Il n\'y a plus de place disponible pour certains e-billets</div>';
        }
    }



    /**
     * Methode qui permet de valider une facture et de choisir
     * son type de paiement (differe ou non)
     *
     * @param $data
     *
     *      Requete du type POST contenant les informations requises
     */

    private function validerFacture($data){
        $f = $_SESSION['facture_en_cours'];
        if(isset($data['mode'])) {

            if($data['mode'] == 'direct') {
                $f->reglee = 0;
            } else {
                $f->reglee = 2;
            }

        }

        $f->typePaiement = 'CB';
        $f->resteAPaye = $f->totalTTC;
        $ebillets = array();
        $f->save();
        foreach($_SESSION['client']->articles as $art){
            $f->articles()->attach($art,array("qte" => $art->pivot->qte));
            if($art->type == 'Ebillet'){
                $art->typeable->reserver();
                $ebillets[] = $art->typeable;
            }
        }

        foreach($_SESSION['client']->articles as $art)
        {
            $art->pivot->delete();
        }

        $_SESSION['client'] = Client::find($_SESSION['client']->email);
        \Slim\Slim::getInstance()->redirectTo('perso');
    }



    /**
     * Merthode qui permet de retirer un article du panier
     *
     * @param $id
     *
     *      Identifiant de l'article a supprimer
     */


    public function supprimerArticle($id)
    {
        if ($this->isConnected()) {
            $client = Client::find($_SESSION['client']->email);
            $articles = $client->articles;
            foreach($articles as $article){
                if($article->numArt == $id)
                {
                    $client->articles()->detach($article);
                    if($article->type != 'Produit') {
                        $article->typeable->delete();
                        $article->delete();
                    }
                }
            }
            $_SESSION['client'] = Client::find($_SESSION['client']->email);
        }
        \Slim\Slim::getInstance()->redirectTo('panier');
    }
}