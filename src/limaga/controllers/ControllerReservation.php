<?php

namespace limaga\controllers;


use limaga\models\Client;
use limaga\models\Article;
use limaga\models\Eabonnement;
use limaga\models\Ebillet;
use limaga\models\Planning;
use limaga\vue\VueEbillet;
use limaga\vue\VueReserver;

class ControllerReservation extends AbstractController{

    /**
     * Merthode permettant d'afficher le formulaire de reservation
     * pour un e-billet en creant la vue associee
     */


    public function afficherFormulaire(){
        $data = $this->request->get();
        if(count($data) > 0){
            if(isset($data['date_journee']) && !empty($data['date_journee']) &&
            isset($data['time']) && !empty($data['time']))
            {
                $this->afficherPlaceRestante($data['date_journee'], $data['time']);
            }
        }else
        {
            $v = new VueReserver('Ebillet');
            $v->render($this->isConnected());
        }
    }




    /**
     * Merthode permettant d'afficher le formulaire de reservation
     * pour un e-abonnement en creant la vue associee
     */


    public function afficherFormulaireEabonnement(){
        $v = new VueReserver('Eabonnement');
        $v->render($this->isConnected());
    }



    /**
     * Methode permettant d'afficher le nombre de places restantes
     * dans la piscine a une date donnee
     *
     * @param $date_journee
     *
     *          Date de la demande de reservation
     *
     * @param $time
     *
     *          Moment de la journee de la demande de reservation
     */

    public function afficherPlaceRestante($date_journee,$time)
    {
        if($this->isConnected()) {
            $demiJournee = Planning::find($date_journee);
            $html = '';
            $dateactuelle = new \DateTime();
            $datereserve = \DateTime::createFromFormat('Y-m-d',$date_journee);
            $diff = $datereserve->getTimestamp() - $dateactuelle->getTimestamp();

            if ($demiJournee != null && $diff >= 0) {
                $entreeDispo = 400;
                if ($time == 'AMPM') {
                    $html .= '<div class="alert alert-success">';
                    $html .= 'Il reste ' . (400 - $demiJournee->entreeAM) . ' entrées disponibles le matin </br>';
                    $html .= 'Il reste ' . (400 - $demiJournee->entreePM) . ' entrées disponibles l\'après midi';
                    $html .= '</div>';
                } else {
                    if ($time == 'AM') {
                        $entreeDispo = 400 - $demiJournee->entreeAM;
                    } else if ($time == 'PM') {
                        $entreeDispo = 400 - $demiJournee->entreePM;
                    }

                    if ($entreeDispo > 0) {
                        $html .= '<div class="alert alert-success">';
                        $html .= "Il reste $entreeDispo entrées disponibles";
                        $html .= '</div>';
                    } else {
                        $html .= '<div class="alert alert-warning">';
                        $html .= 'Il n\'y a plus d\'entrées disponibles';
                        $html .= '</div>';
                    }
                }
            } else {
                $html .= '<div class="alert alert-warning">';
                $html .= 'Vous ne pouvez pas reserver pour cette journée';
                $html .= '</div>';
            }
            echo $html;
        }
    }


    /**
     * Merthode permettant l'ajout d'un e-abonnement dans le panier
     */

    public function ajouterEabonnement(){
        if($this->isConnected()){
            $data = $this->request->post();
            if(isset($data['commanderEabonnement']) && $data['commanderEabonnement'] == 'eabonnement')
            {
                $article = new Article();
                $article->type ='Eabonnement';
                $_SESSION['client']->articles()->save($article,array("qte"=>1));

                $eab = new Eabonnement();
                $eab->numArt = $article->numArt;
                $eab->NbRestant = $data['NbRestant'];
                $eab->NbPlace = $eab->NbRestant;
                $eab->client = $_SESSION['client']->email;
                $eab->save();

                $_SESSION['client'] = Client::find($_SESSION['client']->email);
                \Slim\Slim::getInstance()->redirectTo('panier');
            }
        }
    }

    /**
     * Methode permettant l'ajout d'un e-abonnement dans le panier
     */

    public function ajouterEBillet(){
        if($this->isConnected())
        {
            $data = $this->request->post();
            if (isset($data['commander']) && $data['commander'] == 'f2') {
                $demijournee = Planning::find($data['demijournee']);
                $erreur = false;
                $dateactuelle = new \DateTime();
                $datereserve = \DateTime::createFromFormat('Y-m-d',$data['demijournee']);

                if ($demijournee != null){
                    if($datereserve->getTimestamp() - $dateactuelle->getTimestamp() < 0){
                        $erreur  = true;
                    }else {
                        if (isset($data['checkAM']))
                            $erreur = $demijournee->complet('AM');

                        if (isset($data['checkPM']) && !$erreur)
                            $erreur = $demijournee->complet('PM');
                    }
                } else
                    $erreur = true;
                if (!$erreur) {
                    $articles = [];
                    $ebillets = [];

                    if (isset($data['checkAM'])) {
                        $art = new Article();
                        $art->type = 'Ebillet';
                        $articles[] = $art;
                        $ebi = new Ebillet();
                        $ebi->date = date_create_from_format('Y-m-j', $data['demijournee'])->setTime(8, 0)->format('Y-m-j H');
                        if(isset($data['choixEabonnement']) && $data['choixEabonnement']!=-1){
                            $ebi->id_eabonnement = $data['choixEabonnement'];
                        }
                        $ebillets[] = $ebi;
                    }

                    if (isset($data['checkPM'])) {
                        $art = new Article();
                        $art->type = 'Ebillet';
                        $articles[] = $art;
                        $ebi = new Ebillet();
                        $ebi->date = date_create_from_format('Y-m-j', $data['demijournee'])->setTime(14, 0)->format('Y-m-j H');
                        if(isset($data['choixEabonnement']) && $data['choixEabonnement']!=-1){
                            $ebi->id_eabonnement = $data['choixEabonnement'];
                        }
                        $ebillets[] = $ebi;
                    }

                    $client = $_SESSION['client'];
                    foreach ($articles as $index => $article) {
                        $article->save();
                        $ebillets[$index]->numArt = $article->numArt;
                        $ebillets[$index]->client = $client->email;
                        $ebillets[$index]->save();
                        $client->articles()->attach($article, array("qte" => 1));
                    }


                    $_SESSION['client'] = Client::find($client->email);

                    \Slim\Slim::getInstance()->redirectTo('panier');
                } else {
                    echo 'Erreur impossible de reserver pour cette demi-journée';
                }
            }
        }
    }
}