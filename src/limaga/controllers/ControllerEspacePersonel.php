<?php

namespace limaga\controllers;

use limaga\vue\VueEspacePersonnel;
use limaga\models\Client;

class ControllerEspacePersonel extends AbstractController

{
    /**
     * Methode qui permet d'afficher l'espace personnel d'un
     * utilisateur en creant la vue associee
     */

    public function afficherEspacePersonnel(){

        $client = Client::find($_SESSION['client']->email);

        $vue = new VueEspacePersonnel($client);
        $vue->render($this->isConnected());

    }
}