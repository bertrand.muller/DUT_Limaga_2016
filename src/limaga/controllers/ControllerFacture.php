<?php

namespace limaga\controllers;

use limaga\vue\VueFacture;
use limaga\models\Client;

class ControllerFacture extends AbstractController

{

    /**
     * Methode qui permet d'afficher les factures
     * en creant la vue associee
     */

    public function afficherDetailsFacture($num){

        $client = Client::find($_SESSION['client']->email);

        foreach($client->factures as $fact) {

            if($fact->num == $num) {
                $vue = new VueFacture($fact);
                $vue->render($this->isConnected());
                break;
            }

        }
    }
}