<?php

namespace limaga\controllers;

abstract class AbstractController {

    /**
     * Attribut representant la requete http
     */
    protected $request;

    /**
     * Constrcuteur qui initialise le controllerr avec une requete qui
     * lui sera associee
     *
     * @param $request
     *
     *      Requete a associee avec le controller
     */
    public function __construct($request){
        $this->request = $request;
    }


    /**
     * Methode qui permet de retourner l'etat de connexion d'un
     * utilisateur (connecte ou non)
     *
     * @return bool
     *
     *      Retourne True si l'utilisateur est connecte, False sinon
     */

    public function isConnected(){
        return (isset($_SESSION) && !empty($_SESSION) && isset($_SESSION['client']));
    }
}