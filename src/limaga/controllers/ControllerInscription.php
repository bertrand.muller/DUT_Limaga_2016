<?php

namespace limaga\controllers;

use limaga\vue\VueConnexion as VueConnexion;
use limaga\models\Client as Client;

class ControllerInscription extends AbstractController{

    /**
     * Merhode qui permet a un visiteur de s'inscrire
     * pour devenir client
     */

    public function inscrireClient(){
        $data = $this->request->post();
        if(isset($data['inscription']) && $data['inscription']=='f2')
        {
            $client = new Client();
            $client->nom = $data['nom'];
            $client->prenom = $data['prenom'];
            $client->mdp = MD5($data['mdp']);
            $client->addresse = $data['adresse'];
            $client->dateNaissance = $data['dateNaiss'];
            $client->numTelephone = $data['telephone'];
            $client->niveau = $data['lvl'];
            $client->email = $data['email'];
            $client->save();
            $_SESSION['client'] = Client::find($client->email);
            \Slim\Slim::getInstance()->redirectTo('accueil');
        }else{
            echo 'Inscription échouée';
        }
    }
}