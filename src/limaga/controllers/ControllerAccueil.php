<?php

namespace limaga\controllers;


use limaga\models\Article;
use limaga\models\Ebillet;
use limaga\models\Client;
use limaga\vue\VueAccueil;

class ControllerAccueil extends AbstractController{


    /**
     * Methode permettant d'afficher l'accueil en
     * creant la vue associee
     */

    public function afficherAccueil(){
        $vue = new VueAccueil();
        $vue->render($this->isConnected());
    }
}