<?php

namespace limaga\models;


use Illuminate\Database\Eloquent\Model;

/**
 * Class Planning
 * Represente la table planning soit un jour avec le nombre d'entrée disponnible
 * matin et après midi
 * @package limaga\models
 */
class Planning extends Model{

    protected $table='Planning';
    protected $primaryKey='dateJournee';
    public $timestamps=false;

    /**
     * Permet de savoir si il reste des places disponnibles le matin ou l'après midi
     * @param $time
     *  AM matin
     *  PM après midi
     * @return bool
     */
    public function complet($time){
        if($time == 'PM'){
            return ($this->entreePM >= MAXENTREE);
        }else if($time == 'AM'){
            return ($this->entreeAM >= MAXENTREE);
        }else {
            return true;
        }
    }
}