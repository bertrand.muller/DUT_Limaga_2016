<?php

namespace limaga\models;

use \Illuminate\Database\Eloquent\Model;

/**
 * Class Article
 * Représente tout objets qui peut être acheté
 * @package limaga\models
 */
class Article extends Model{
    protected $morphClass= 'Article';

    protected $table='Article';
    protected $primaryKey='numArt';
    public $timestamps=false;

    /**
     * Transforme l'article en un article concret (par exemple un E-billet)
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function typeable(){
        return $this->morphTo(null,'type','numArt');
    }

    /**
     * Récupère le tarif de l'article dans la table tarif
     * @return Tarif
     */
    public function tarif(){
        return $this->belongsTo('\limaga\models\Tarif','type');
    }

    /**
     * Retourne les clients qui ont dans leur panier cet article
     * @return clients
     */
    public function clients(){
        return $this->belongsToMany('\limaga\models\Client','Panier','numArt','email')->withPivot('qte');
    }

    /**
     * Instancie la vue associé à l'article (en fonction du type)
     * @return VueArticle
     */
    public function getVue(){
        $vue_construct = '\\limaga\\vue\\articles\\Vue'.$this->type;
        return new $vue_construct($this->typeable);
    }
}