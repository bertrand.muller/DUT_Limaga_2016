<?php

namespace limaga\models;

use \Illuminate\Database\Eloquent\Model;

/**
 * Class Client
 * Représente un client dans la base de données
 * @package limaga\models
 */
class Client extends Model{

    protected $table='Client';
    protected $primaryKey='email';
    public $timestamps=false;

    /**
     * Retourne les articles contenu dans le panier
     * @return articles
     */
    public function articles(){
        return $this->belongsToMany('\limaga\models\Article','Panier','email','numArt')->withPivot('qte');
    }

    /**
     * Retourne les eabonnements associé au client
     * @return Eabonnements
     */
    public function eabonnements(){
        return $this->hasMany('\limaga\models\Eabonnement','client','email');
    }

    /**
     * Retourne toutes les factures du client
     * @return Factures
     */
    public function factures(){
        return $this->hasMany('\limaga\models\Facture','email','email');
    }

    /**
     * Retourne tout les e-billets associé au client
     * @return Ebillets
     */
    public function ebillets(){
        return $this->hasMany('\limaga\models\Ebillet','client','email');
    }

    public function getNiveauNatation(){


        $niv = '';

        switch($this->getAttribute('niveau')){

            case 0:

                $niv = 'Débutant';
                break;

            case 1 :

                $niv = 'Intermédiaire';
                break;

            default:

                $niv = 'Expérimenté';
                break;
        }

        return $niv;
    }


}