<?php

namespace limaga\models;


use Illuminate\Database\Eloquent\Model;

/**
 * Class Facture
 * Représente une Facture dans la base de donnée
 * @package limaga\models
 */
class Facture extends Model{

    protected $table='Facture';
    protected $primaryKey='num';
    public $timestamps=false;

    /**
     * Retourne le contenu de la facture
     * @return articles
     */
    public function articles(){
        return $this->belongsToMany('\limaga\models\Article','ArticlesFactures','numFact','numArt')->withPivot('qte');
    }

    /**
     * Retourne le client associé a la facture
     * @return mixed
     */
    public function client(){
        return $this->belongsTo('\limaga\models\Client','email')->getResults();
    }
}