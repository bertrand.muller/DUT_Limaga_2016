<?php

namespace limaga\models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Ebillet
 * Représente un E-billet qui peut être échanger contre un bracelet a la piscine
 * @package limaga\models
 */
class Ebillet extends Model{
    protected $morphClass= 'Ebillet';

    protected $table='Ebillet';
    protected $primaryKey='numArt';
    public $timestamps=false;

    /**
     * Verifie les paramètres du e-billet
     * @return boolean
     */
    public function estDisponible(){
        $date = new \DateTime($this->date);
        $h = $date->format('H');
        $p = Planning::find($date->setTime(0,0,0));
        /* Si la date n'est pas dans planning */
        if($p == null)
            return false;

        if($this->eabonnement != null){
            /* Si le e-billet est associé a un e-abonnement et que celui-ci est périmé */
            if($this->eabonnement->NbRestant <= 0){
                return false;
            }
        }

        /* Vérification du nombre d'entrée */
        if($h == '14')
        {
            return ($p->entreePM < MAXENTREE);
        }else if($h == '8')
        {
            return ($p->entreeAM < MAXENTREE);
        }else
            return false;
    }

    /**
     * Permet d'effectuer la reservation du e-billet
     * soit la mise a jour du nombre de place disponnible dans la piscine
     * et de décrementer eventuellement l'e-abonnement associé
     */
    public function reserver(){
        $date = new \DateTime($this->date);
        $h = $date->format('H');
        $p = Planning::find($date->setTime(0,0,0));
        if($p != null){
            if($h == '14')
            {
                $p->entreePM+=1;
            }else if($h == '8')
            {
                $p->entreeAM+=1;
            }
            $p->save();
        }
        if($this->eabonnement != null){
            if($this->eabonnement->NbRestant > 0){
                $this->eabonnement->NbRestant -= 1;
                $this->eabonnement->save();
            }
        }
    }

    /**
     * Renvoie l'Article de la table parent
     * @return Article
     */
    public function article(){
        return $this->morphOne('\limaga\models\Article','typeable','type','numArt');
    }

    /**
     * Retourne le client associé à l'e-billet
     * @return Client
     */
    public function client(){
        return $this->belongsTo('\limaga\models\Client','client');
    }

    /**
     * Retourne l'eabonnement utilisé pour acheter l'e-billet (null si inexistant)
     * @return Eabonnement
     */
    public function eabonnement(){
        return $this->belongsTo('\limaga\models\Eabonnement','id_eabonnement');
    }

    /**
     * Calcul le prix de l'e-billet (0 si un e-abonnement est utilisé)
     * @return prix
     */
    public function prix(){
        if(isset($this->id_eabonnement) && $this->id_eabonnement > 0)
            return 0;
        else
            return $this->article->tarif->tarifDemiJournee;
    }
}