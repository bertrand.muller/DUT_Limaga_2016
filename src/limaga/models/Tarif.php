<?php

namespace limaga\models;


use Illuminate\Database\Eloquent\Model;

/**
 * Class Tarif
 * Représente un tarif pour un type de reservation
 * @package limaga\models
 */
class Tarif extends Model{

    protected $table='Tarif';
    protected $primaryKey='type';
    public $timestamps=false;

    /**
     * Retourne tous les articles qui ont ce tarif
     * @return Article
     */
    public function article(){
        return $this->hasMany('\limaga\models\Article','type');
    }
}