<?php

namespace limaga\models;


use Illuminate\Database\Eloquent\Model;

/**
 * Class Eabonnement
 * Représente un E-abonnement
 * Un E-abonnement possède un nombre de place gratuite qui
 * décremente lors de l'achat d'un E-billet avec cet e-abonnement
 * @package limaga\models
 */
class Eabonnement extends Model{

    protected $morphClass= 'Eabonnement';

    protected $table='Eabonnement';
    protected $primaryKey='numArt';
    public $timestamps=false;

    /**
     * Retourne le contenu de la table parent (Article)
     * @return Article
     */
    public function article(){
        return $this->morphOne('\limaga\models\Article','typeable','type','numArt');
    }

    /**
     * Retourne le client associé a cet e-abonnement
     * @return Client
     */
    public function client(){
        return $this->belongsTo('\limaga\models\Client','client');
    }

    /**
     * Retourne le prix de l'eabonnement en fonction du nombre et du tarif
     * @return prix
     */
    public function prix(){
        return $this->article->tarif->tarifDemiJournee * $this->NbPlace/10.0;
    }
}