<?php

namespace limaga\models;


use Illuminate\Database\Eloquent\Model;

/**
 * Class Produit
 * Représente un produit dérivé
 * @package limaga\models
 */
class Produit extends Model{
    protected $morphClass= 'Produit';

    protected $table='Produit';
    protected $primaryKey='numArt';
    public $timestamps=false;

    /**
     * Retourne l'article parent
     * @return Article
     */
    public function article(){
        return $this->morphOne('\limaga\models\Article','typeable','type','numArt');
    }

    /**
     * Retourne le prix du produit
     * @return prix
     */
    public function prix(){
        return $this->prix;
    }
}