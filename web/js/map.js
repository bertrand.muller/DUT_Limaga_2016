/**
 * Created by MathieuBourgault on 13/01/2016.
 */
function initialize() {
    var centre =new google.maps.LatLng(48.682823, 6.161117);

    var mapProp = {
        center:centre,
        zoom:17,
        mapTypeControlOptions: {
            style:google.maps.MapTypeControlStyle.DROPDOWN_MENU
        },
        mapTypeId:google.maps.MapTypeId.ROADMAP
    };

    var map=new google.maps.Map(document.getElementById("googleMap"),mapProp);

    var marker=new google.maps.Marker({
        position:centre,
    });

    marker.setMap(map);

}google.maps.event.addDomListener(window, 'load', initialize);