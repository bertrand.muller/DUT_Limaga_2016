<?php
/**
 * Created by PhpStorm.
 * User: thomas
 * Date: 10/12/15
 * Time: 20:02
 */


require 'vendor/autoload.php';

use Illuminate\Database\Capsule\Manager as DB;

define('CONNECTE',true);
define('DECONNECTE',false);
define('MAXENTREE',400);
define('TVA',0.2);

class_alias('\limaga\models\Article','Article');
class_alias('\limaga\models\Ebillet','Ebillet');
class_alias('\limaga\models\Produit','Produit');
class_alias('\limaga\models\Eabonnement','Eabonnement');

session_start();


$db = new DB();
$db->addConnection(parse_ini_file('src/conf/db.limaga.conf.ini'));
$db->setAsGlobal();
$db->bootEloquent();

$app = new \Slim\Slim();

$app->get('/',function() use($app){
    $app->redirectTo('accueil');
});

$app->get('/accueil',function () use($app){
    $c = new \limaga\controllers\ControllerAccueil($app->request());
    $c->afficherAccueil();
})->name('accueil');

$app->get('/connexion',function() use($app){
    $c = new \limaga\controllers\ControllerConnexion($app->request());
    $c->formulaireConnexion();
})->name('connexion');

$app->get('/deconnexion',function() use($app){
    session_destroy();
    $app->redirectTo('accueil');
})->name('deconnexion');

$app->post('/inscription', function() use($app){
    $c = new \limaga\controllers\ControllerInscription($app->request());
    $c->inscrireClient();
});

$app->post('/connexion', function() use($app){
    $c = new limaga\controllers\ControllerConnexion($app->request());
    $c->seConnecte();
});

$app->get('/achat',function() use($app){
    $c = new limaga\controllers\ControllerAchat($app->request());
    $c->listerProduits();
})->name('catalogue');

$app->post('/achat/:id',function($id) use($app){
    $c = new limaga\controllers\ControllerAchat($app->request());
    $c->acheterProduit($id);
});

$app->get('/reserver/Ebillet',function() use($app){
    $c = new limaga\controllers\ControllerReservation($app->request());
    $c->afficherFormulaire();
});

$app->post('/reserver/Ebillet',function() use($app){
   $c = new limaga\controllers\ControllerReservation($app->request());
   $c->ajouterEBillet();
});

$app->get('/reserver/Eabonnement',function() use($app){
    $c = new limaga\controllers\ControllerReservation($app->request());
    $c->afficherFormulaireEabonnement();
});

$app->post('/reserver/Eabonnement',function() use($app){
    $c = new limaga\controllers\ControllerReservation($app->request());
    $c->ajouterEabonnement();
});


$app->get('/panier',function() use($app){
   $c = new limaga\controllers\ControllerPanier($app->request());
   $c->lister();
})->name('panier');

$app->post('/panier',function() use($app){
   $c = new limaga\controllers\ControllerPanier($app->request());
   $c->commander();
});

$app->post('/panier/:id',function($id) use($app){
    $c = new limaga\controllers\ControllerPanier($app->request());
    $c->supprimerArticle($id);
});


$app->get('/perso',function() use($app){
    $c = new limaga\controllers\ControllerEspacePersonel($app->request());
    $c->afficherEspacePersonnel();
})->name('perso');

$app->post('/perso/fact/:id',function($id) use($app){
    $c = new limaga\controllers\ControllerFacture($app->request());
    $c->afficherDetailsFacture($id);
});

$app->get('/contact',function() use($app){
    $c = new limaga\controllers\ControllerContact($app->request());
    $c->afficherInformationsContact();
});

$app->run();
