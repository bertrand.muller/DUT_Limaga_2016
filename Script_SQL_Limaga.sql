-- phpMyAdmin SQL Dump
-- version 4.2.6deb1
-- http://www.phpmyadmin.net
--
-- Client :  localhost
-- Généré le :  Dim 17 Janvier 2016 à 15:19
-- Version du serveur :  5.5.44-0ubuntu0.14.10.1-log
-- Version de PHP :  5.5.12-2ubuntu4.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `Limaga`
--

-- --------------------------------------------------------

--
-- Structure de la table `Article`
--

CREATE TABLE IF NOT EXISTS `Article` (
`numArt` int(11) NOT NULL,
  `type` varchar(128) DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Contenu de la table `Article`
--

INSERT INTO `Article` (`numArt`, `type`) VALUES
(1, 'Produit'),
(2, 'Produit');

-- --------------------------------------------------------

--
-- Structure de la table `ArticlesFactures`
--

CREATE TABLE IF NOT EXISTS `ArticlesFactures` (
  `numFact` int(11) NOT NULL,
  `numArt` int(11) NOT NULL,
  `qte` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `Client`
--

CREATE TABLE IF NOT EXISTS `Client` (
  `email` varchar(64) NOT NULL,
  `nom` varchar(32) DEFAULT NULL,
  `prenom` varchar(32) DEFAULT NULL,
  `dateNaissance` date DEFAULT NULL,
  `numTelephone` varchar(16) DEFAULT NULL,
  `niveau` int(2) DEFAULT NULL,
  `mdp` varchar(32) DEFAULT NULL,
  `addresse` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `Client`
--

INSERT INTO `Client` (`email`, `nom`, `prenom`, `dateNaissance`, `numTelephone`, `niveau`, `mdp`, `addresse`) VALUES
('mail@mail.mail', 'Jean', 'Michel', '2016-01-01', '00002000', 0, 'cc03e747a6afbbcbf8be7668acfebee5', ' 0 rue bidon bidonville 00000');

-- --------------------------------------------------------

--
-- Structure de la table `Eabonnement`
--

CREATE TABLE IF NOT EXISTS `Eabonnement` (
  `NbRestant` int(3) NOT NULL,
  `client` varchar(64) NOT NULL,
  `numArt` int(11) NOT NULL DEFAULT '0',
  `NbPlace` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `Ebillet`
--

CREATE TABLE IF NOT EXISTS `Ebillet` (
  `numArt` int(11) NOT NULL DEFAULT '0',
  `date` datetime DEFAULT NULL,
  `client` varchar(64) NOT NULL,
  `id_eabonnement` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `Facture`
--

CREATE TABLE IF NOT EXISTS `Facture` (
`num` int(11) NOT NULL,
  `email` varchar(64) NOT NULL,
  `date` date NOT NULL,
  `totalHT` decimal(8,2) NOT NULL,
  `totalTTC` decimal(8,2) NOT NULL,
  `reglee` int(2) NOT NULL,
  `typePaiement` varchar(32) NOT NULL,
  `resteAPaye` double(5,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `Panier`
--

CREATE TABLE IF NOT EXISTS `Panier` (
  `email` varchar(64) NOT NULL DEFAULT '',
  `numArt` int(11) NOT NULL DEFAULT '0',
  `qte` int(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `Planning`
--

CREATE TABLE IF NOT EXISTS `Planning` (
  `dateJournee` date NOT NULL,
  `entreePM` int(11) NOT NULL,
  `entreeAM` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `Planning`
--

INSERT INTO `Planning` (`dateJournee`, `entreePM`, `entreeAM`) VALUES
('2016-01-01', 0, 0),
('2016-01-02', 0, 0),
('2016-01-03', 0, 0),
('2016-01-04', 0, 0),
('2016-01-05', 0, 0),
('2016-01-06', 0, 0),
('2016-01-07', 0, 0),
('2016-01-08', 0, 0),
('2016-01-09', 0, 0),
('2016-01-10', 0, 0),
('2016-01-11', 0, 0),
('2016-01-12', 0, 0),
('2016-01-13', 0, 0),
('2016-01-14', 0, 0),
('2016-01-15', 0, 0),
('2016-01-16', 0, 0),
('2016-01-17', 0, 0),
('2016-01-18', 0, 0),
('2016-01-19', 0, 0),
('2016-01-20', 0, 0),
('2016-01-21', 0, 0),
('2016-01-22', 0, 0),
('2016-01-23', 0, 0),
('2016-01-24', 0, 0),
('2016-01-25', 0, 0),
('2016-01-26', 0, 1),
('2016-01-27', 0, 0),
('2016-01-28', 1, 0),
('2016-01-29', 0, 0),
('2016-01-30', 0, 0),
('2016-01-31', 0, 0),
('2016-02-01', 0, 0),
('2016-02-02', 0, 0),
('2016-02-03', 0, 0),
('2016-02-04', 0, 0),
('2016-02-05', 0, 0),
('2016-02-06', 0, 0),
('2016-02-07', 0, 0),
('2016-02-08', 0, 0),
('2016-02-09', 0, 0),
('2016-02-10', 0, 0),
('2016-02-11', 0, 0),
('2016-02-12', 0, 0),
('2016-02-13', 0, 0),
('2016-02-14', 0, 0),
('2016-02-15', 0, 0),
('2016-02-16', 0, 0),
('2016-02-17', 0, 0),
('2016-02-18', 0, 0),
('2016-02-19', 0, 0),
('2016-02-20', 0, 0),
('2016-02-21', 0, 0),
('2016-02-22', 0, 0),
('2016-02-23', 0, 0),
('2016-02-24', 0, 0),
('2016-02-25', 0, 0),
('2016-02-26', 0, 0),
('2016-02-27', 0, 0),
('2016-02-28', 0, 0),
('2016-02-29', 0, 0),
('2016-03-01', 0, 0),
('2016-03-02', 0, 0),
('2016-03-03', 0, 0),
('2016-03-04', 0, 0),
('2016-03-05', 0, 0),
('2016-03-06', 0, 0),
('2016-03-07', 0, 0),
('2016-03-08', 0, 0),
('2016-03-09', 0, 0),
('2016-03-10', 0, 0),
('2016-03-11', 0, 0),
('2016-03-12', 0, 0),
('2016-03-13', 0, 0),
('2016-03-14', 0, 0),
('2016-03-15', 0, 0),
('2016-03-16', 0, 0),
('2016-03-17', 0, 0),
('2016-03-18', 0, 0),
('2016-03-19', 0, 0),
('2016-03-20', 0, 0),
('2016-03-21', 0, 0),
('2016-03-22', 0, 0),
('2016-03-23', 0, 0),
('2016-03-24', 0, 0),
('2016-03-25', 0, 0),
('2016-03-26', 0, 0),
('2016-03-27', 0, 0),
('2016-03-28', 0, 0),
('2016-03-29', 0, 0),
('2016-03-30', 0, 0),
('2016-03-31', 0, 0),
('2016-04-01', 0, 0),
('2016-04-02', 0, 0),
('2016-04-03', 0, 0),
('2016-04-04', 0, 0),
('2016-04-05', 0, 0),
('2016-04-06', 0, 0),
('2016-04-07', 0, 0),
('2016-04-08', 0, 0),
('2016-04-09', 0, 0),
('2016-04-10', 0, 0),
('2016-04-11', 0, 0),
('2016-04-12', 0, 0),
('2016-04-13', 0, 0),
('2016-04-14', 0, 0),
('2016-04-15', 0, 0),
('2016-04-16', 0, 0),
('2016-04-17', 0, 0),
('2016-04-18', 0, 0),
('2016-04-19', 0, 0),
('2016-04-20', 0, 0),
('2016-04-21', 0, 0),
('2016-04-22', 0, 0),
('2016-04-23', 0, 0),
('2016-04-24', 0, 0),
('2016-04-25', 0, 0),
('2016-04-26', 0, 0),
('2016-04-27', 0, 0),
('2016-04-28', 0, 0),
('2016-04-29', 0, 0),
('2016-04-30', 0, 0),
('2016-05-01', 0, 0),
('2016-05-02', 0, 0),
('2016-05-03', 0, 0),
('2016-05-04', 0, 0),
('2016-05-05', 0, 0),
('2016-05-06', 0, 0),
('2016-05-07', 0, 0),
('2016-05-08', 0, 0),
('2016-05-09', 0, 0),
('2016-05-10', 0, 0),
('2016-05-11', 0, 0),
('2016-05-12', 0, 0),
('2016-05-13', 0, 0),
('2016-05-14', 0, 0),
('2016-05-15', 0, 0),
('2016-05-16', 0, 0),
('2016-05-17', 0, 0),
('2016-05-18', 0, 0),
('2016-05-19', 0, 0),
('2016-05-20', 0, 0),
('2016-05-21', 0, 0),
('2016-05-22', 0, 0),
('2016-05-23', 0, 0),
('2016-05-24', 0, 0),
('2016-05-25', 0, 0),
('2016-05-26', 0, 0),
('2016-05-27', 0, 0),
('2016-05-28', 0, 0),
('2016-05-29', 0, 0),
('2016-05-30', 0, 0),
('2016-05-31', 0, 0),
('2016-06-01', 0, 0),
('2016-06-02', 0, 0),
('2016-06-03', 0, 0),
('2016-06-04', 0, 0),
('2016-06-05', 0, 0),
('2016-06-06', 0, 0),
('2016-06-07', 0, 0),
('2016-06-08', 0, 0),
('2016-06-09', 0, 0),
('2016-06-10', 0, 0),
('2016-06-11', 0, 0),
('2016-06-12', 0, 0),
('2016-06-13', 0, 0),
('2016-06-14', 0, 0),
('2016-06-15', 0, 0),
('2016-06-16', 0, 0),
('2016-06-17', 0, 0),
('2016-06-18', 0, 0),
('2016-06-19', 0, 0),
('2016-06-20', 0, 0),
('2016-06-21', 0, 0),
('2016-06-22', 0, 0),
('2016-06-23', 0, 0),
('2016-06-24', 0, 0),
('2016-06-25', 0, 0),
('2016-06-26', 0, 0),
('2016-06-27', 0, 0),
('2016-06-28', 0, 0),
('2016-06-29', 0, 0),
('2016-06-30', 0, 0),
('2016-07-01', 0, 0),
('2016-07-02', 0, 0),
('2016-07-03', 0, 0),
('2016-07-04', 0, 0),
('2016-07-05', 0, 0),
('2016-07-06', 0, 0),
('2016-07-07', 0, 0),
('2016-07-08', 0, 0),
('2016-07-09', 0, 0),
('2016-07-10', 0, 0),
('2016-07-11', 0, 0),
('2016-07-12', 0, 0),
('2016-07-13', 0, 0),
('2016-07-14', 0, 0),
('2016-07-15', 0, 0),
('2016-07-16', 0, 0),
('2016-07-17', 0, 0),
('2016-07-18', 0, 0),
('2016-07-19', 0, 0),
('2016-07-20', 0, 0),
('2016-07-21', 0, 0),
('2016-07-22', 0, 0),
('2016-07-23', 0, 0),
('2016-07-24', 0, 0),
('2016-07-25', 0, 0),
('2016-07-26', 0, 0),
('2016-07-27', 0, 0),
('2016-07-28', 0, 0),
('2016-07-29', 0, 0),
('2016-07-30', 0, 0),
('2016-07-31', 0, 0),
('2016-08-01', 0, 0),
('2016-08-02', 0, 0),
('2016-08-03', 0, 0),
('2016-08-04', 0, 0),
('2016-08-05', 0, 0),
('2016-08-06', 0, 0),
('2016-08-07', 0, 0),
('2016-08-08', 0, 0),
('2016-08-09', 0, 0),
('2016-08-10', 0, 0),
('2016-08-11', 0, 0),
('2016-08-12', 0, 0),
('2016-08-13', 0, 0),
('2016-08-14', 0, 0),
('2016-08-15', 0, 0),
('2016-08-16', 0, 0),
('2016-08-17', 0, 0),
('2016-08-18', 0, 0),
('2016-08-19', 0, 0),
('2016-08-20', 0, 0),
('2016-08-21', 0, 0),
('2016-08-22', 0, 0),
('2016-08-23', 0, 0),
('2016-08-24', 0, 0),
('2016-08-25', 0, 0),
('2016-08-26', 0, 0),
('2016-08-27', 0, 0),
('2016-08-28', 0, 0),
('2016-08-29', 0, 0),
('2016-08-30', 0, 0),
('2016-08-31', 0, 0),
('2016-09-01', 0, 0),
('2016-09-02', 0, 0),
('2016-09-03', 0, 0),
('2016-09-04', 0, 0),
('2016-09-05', 0, 0),
('2016-09-06', 0, 0),
('2016-09-07', 0, 0),
('2016-09-08', 0, 0),
('2016-09-09', 0, 0),
('2016-09-10', 0, 0),
('2016-09-11', 0, 0),
('2016-09-12', 0, 0),
('2016-09-13', 0, 0),
('2016-09-14', 0, 0),
('2016-09-15', 0, 0),
('2016-09-16', 0, 0),
('2016-09-17', 0, 0),
('2016-09-18', 0, 0),
('2016-09-19', 0, 0),
('2016-09-20', 0, 0),
('2016-09-21', 0, 0),
('2016-09-22', 0, 0),
('2016-09-23', 0, 0),
('2016-09-24', 0, 0),
('2016-09-25', 0, 0),
('2016-09-26', 0, 0),
('2016-09-27', 0, 0),
('2016-09-28', 0, 0),
('2016-09-29', 0, 0),
('2016-09-30', 0, 0),
('2016-10-01', 0, 0),
('2016-10-02', 0, 0),
('2016-10-03', 0, 0),
('2016-10-04', 0, 0),
('2016-10-05', 0, 0),
('2016-10-06', 0, 0),
('2016-10-07', 0, 0),
('2016-10-08', 0, 0),
('2016-10-09', 0, 0),
('2016-10-10', 0, 0),
('2016-10-11', 0, 0),
('2016-10-12', 0, 0),
('2016-10-13', 0, 0),
('2016-10-14', 0, 0),
('2016-10-15', 0, 0),
('2016-10-16', 0, 0),
('2016-10-17', 0, 0),
('2016-10-18', 0, 0),
('2016-10-19', 0, 0),
('2016-10-20', 0, 0),
('2016-10-21', 0, 0),
('2016-10-22', 0, 0),
('2016-10-23', 0, 0),
('2016-10-24', 0, 0),
('2016-10-25', 0, 0),
('2016-10-26', 0, 0),
('2016-10-27', 0, 0),
('2016-10-28', 0, 0),
('2016-10-29', 0, 0),
('2016-10-30', 0, 0),
('2016-10-31', 0, 0),
('2016-11-01', 0, 0),
('2016-11-02', 0, 0),
('2016-11-03', 0, 0),
('2016-11-04', 0, 0),
('2016-11-05', 0, 0),
('2016-11-06', 0, 0),
('2016-11-07', 0, 0),
('2016-11-08', 0, 0),
('2016-11-09', 0, 0),
('2016-11-10', 0, 0),
('2016-11-11', 0, 0),
('2016-11-12', 0, 0),
('2016-11-13', 0, 0),
('2016-11-14', 0, 0),
('2016-11-15', 0, 0),
('2016-11-16', 0, 0),
('2016-11-17', 0, 0),
('2016-11-18', 0, 0),
('2016-11-19', 0, 0),
('2016-11-20', 0, 0),
('2016-11-21', 0, 0),
('2016-11-22', 0, 0),
('2016-11-23', 0, 0),
('2016-11-24', 0, 0),
('2016-11-25', 0, 0),
('2016-11-26', 0, 0),
('2016-11-27', 0, 0),
('2016-11-28', 0, 0),
('2016-11-29', 0, 0),
('2016-11-30', 0, 0),
('2016-12-01', 0, 0),
('2016-12-02', 0, 0),
('2016-12-03', 0, 0),
('2016-12-04', 0, 0),
('2016-12-05', 0, 0),
('2016-12-06', 0, 0),
('2016-12-07', 0, 0),
('2016-12-08', 0, 0),
('2016-12-09', 0, 0),
('2016-12-10', 0, 0),
('2016-12-11', 0, 0),
('2016-12-12', 0, 0),
('2016-12-13', 0, 0),
('2016-12-14', 0, 0),
('2016-12-15', 0, 0),
('2016-12-16', 0, 0),
('2016-12-17', 0, 0),
('2016-12-18', 0, 0),
('2016-12-19', 0, 0),
('2016-12-20', 0, 0),
('2016-12-21', 0, 0),
('2016-12-22', 0, 0),
('2016-12-23', 0, 0),
('2016-12-24', 0, 0),
('2016-12-25', 0, 0),
('2016-12-26', 0, 0),
('2016-12-27', 0, 0),
('2016-12-28', 0, 0),
('2016-12-29', 0, 0),
('2016-12-30', 0, 0),
('2016-12-31', 0, 0);

-- --------------------------------------------------------

--
-- Structure de la table `Produit`
--

CREATE TABLE IF NOT EXISTS `Produit` (
  `numArt` int(11) NOT NULL,
  `libelle` varchar(64) DEFAULT NULL,
  `prix` double(6,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `Produit`
--

INSERT INTO `Produit` (`numArt`, `libelle`, `prix`) VALUES
(1, 'Lunettes', 10.00),
(2, 'Palmes', 50.00);

-- --------------------------------------------------------

--
-- Structure de la table `Tarif`
--

CREATE TABLE IF NOT EXISTS `Tarif` (
  `type` varchar(128) NOT NULL,
  `tarifDemiJournee` decimal(6,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `Tarif`
--

INSERT INTO `Tarif` (`type`, `tarifDemiJournee`) VALUES
('Eabonnement', 9.00),
('Ebillet', 1.00),
('Produit', 0.00);

--
-- Index pour les tables exportées
--

--
-- Index pour la table `Article`
--
ALTER TABLE `Article`
 ADD PRIMARY KEY (`numArt`), ADD KEY `type` (`type`);

--
-- Index pour la table `ArticlesFactures`
--
ALTER TABLE `ArticlesFactures`
 ADD PRIMARY KEY (`numFact`,`numArt`), ADD KEY `fk_numArt` (`numArt`);

--
-- Index pour la table `Client`
--
ALTER TABLE `Client`
 ADD PRIMARY KEY (`email`);

--
-- Index pour la table `Eabonnement`
--
ALTER TABLE `Eabonnement`
 ADD PRIMARY KEY (`numArt`), ADD KEY `client` (`client`), ADD KEY `numArt` (`numArt`);

--
-- Index pour la table `Ebillet`
--
ALTER TABLE `Ebillet`
 ADD PRIMARY KEY (`numArt`), ADD KEY `client` (`client`), ADD KEY `id_eabonnement` (`id_eabonnement`);

--
-- Index pour la table `Facture`
--
ALTER TABLE `Facture`
 ADD PRIMARY KEY (`num`), ADD KEY `email` (`email`);

--
-- Index pour la table `Panier`
--
ALTER TABLE `Panier`
 ADD PRIMARY KEY (`email`,`numArt`), ADD KEY `numArt` (`numArt`);

--
-- Index pour la table `Planning`
--
ALTER TABLE `Planning`
 ADD PRIMARY KEY (`dateJournee`);

--
-- Index pour la table `Produit`
--
ALTER TABLE `Produit`
 ADD PRIMARY KEY (`numArt`);

--
-- Index pour la table `Tarif`
--
ALTER TABLE `Tarif`
 ADD PRIMARY KEY (`type`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `Article`
--
ALTER TABLE `Article`
MODIFY `numArt` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT pour la table `Facture`
--
ALTER TABLE `Facture`
MODIFY `num` int(11) NOT NULL AUTO_INCREMENT;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `Article`
--
ALTER TABLE `Article`
ADD CONSTRAINT `Article_ibfk_1` FOREIGN KEY (`type`) REFERENCES `Tarif` (`type`);

--
-- Contraintes pour la table `ArticlesFactures`
--
ALTER TABLE `ArticlesFactures`
ADD CONSTRAINT `fk_numArt` FOREIGN KEY (`numArt`) REFERENCES `Article` (`numArt`),
ADD CONSTRAINT `fk_numFact` FOREIGN KEY (`numFact`) REFERENCES `Facture` (`num`);

--
-- Contraintes pour la table `Eabonnement`
--
ALTER TABLE `Eabonnement`
ADD CONSTRAINT `fk_Art` FOREIGN KEY (`numArt`) REFERENCES `Article` (`numArt`),
ADD CONSTRAINT `fk_emailCli` FOREIGN KEY (`client`) REFERENCES `Client` (`email`);

--
-- Contraintes pour la table `Ebillet`
--
ALTER TABLE `Ebillet`
ADD CONSTRAINT `Ebillet_ibfk_2` FOREIGN KEY (`id_eabonnement`) REFERENCES `Eabonnement` (`numArt`),
ADD CONSTRAINT `Ebillet_ibfk_1` FOREIGN KEY (`numArt`) REFERENCES `Article` (`numArt`),
ADD CONSTRAINT `fk_numCli` FOREIGN KEY (`client`) REFERENCES `Client` (`email`);

--
-- Contraintes pour la table `Facture`
--
ALTER TABLE `Facture`
ADD CONSTRAINT `Facture_ibfk_1` FOREIGN KEY (`email`) REFERENCES `Client` (`email`);

--
-- Contraintes pour la table `Panier`
--
ALTER TABLE `Panier`
ADD CONSTRAINT `Panier_ibfk_1` FOREIGN KEY (`email`) REFERENCES `Client` (`email`),
ADD CONSTRAINT `Panier_ibfk_2` FOREIGN KEY (`numArt`) REFERENCES `Article` (`numArt`);

--
-- Contraintes pour la table `Produit`
--
ALTER TABLE `Produit`
ADD CONSTRAINT `Produit_ibfk_1` FOREIGN KEY (`numArt`) REFERENCES `Article` (`numArt`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
